$(document).ready(function () {
//    window.onscroll = function () {
//        myFunction()
//    };
//
//    var header = document.getElementById("sticky-header");
//    var sticky = header.offsetTop;
//
//    function myFunction() {
//        if (window.pageYOffset >= sticky) {
//            header.classList.add("sticky");
//            $('.header-logo-link').css('margin', '10px 0');
//            $('.header-search-form').css('margin-top','27px');
//            $('.btn-shopping-cart').css('margin-top','31px');
//            $('.main-content').css('padding-top', '200px');
//        } else {
//            header.classList.remove("sticky");
//            $('.header-logo-link').css('margin', '20px 0');
//            $('.header-search-form').css('margin-top','37px');
//            $('.btn-shopping-cart').css('margin-top','40px');
//            $('.main-content').css('padding-top', '0px');
//        }
//    }
    
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.header-menu').slideUp();
            $('.hotline').fadeIn();
            $('.header-logo-link').css({'margin': '10px 0'});
            $('.header-logo-link > img').css({'width': '60px'});
            $('.header-search-form').css('margin-top','17px');
            $('.btn-shopping-cart').css('margin-top','21px');
//            $('.main-content').css('padding-top', '200px');
        } 
        if($(this).scrollTop() == 0) {
            $('.header-menu').slideDown();
            $('.hotline').fadeOut();
            $('.header-logo-link').css('margin', '20px 0');
            $('.header-logo-link > img').css({'width': '80px'});
            $('.header-search-form').css('margin-top','37px');
            $('.btn-shopping-cart').css('margin-top','40px');
//            $('.main-content').css('padding-top', '0px');
        }
    });
    // GO TOP
    //Show or hide "#go-top"
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 700) {			
            jQuery('#go-top').fadeIn(200);
        } else {
            jQuery('#go-top').fadeOut(200);
        }
    });
    // Animate "#go-top"
    jQuery('#go-top').click(function (event) {
        event.preventDefault();
        jQuery('html, body').animate({
            scrollTop: 0
        }, '2000', 'swing');
    });
    
    // $(window).load(function(event) {
    //         // Animate loader off screen
    //         $('.loader').show();
            
    //         $('.loader').hide();

    //     });

    // $('.add-to-cart').click(function () {
        
    // });
    $('.products-list-content').on('click', '.add-to-cart', function() {
        productid = $(this).data('productid');
        console.log('yes');
        $.ajax({
            url: BASE_URL + 'product/addtocart',
            type: 'post',
            dataType: 'json',
            data: {'product_id': productid},
            success: function (res) {
                if (!res.error) {

                    updateCartNotiQty(res.qty);
                    $.dialog({
                        type: 'green',
                        title: '',
                        content: res.html,
                        columnClass: 'large'
                    });
                } else {
                    $.dialog({
                        type: 'red',
                        title: 'Lỗi',
                        content: res.error,
                        columnClass: 'medium'
                    });
                }
            }
        });
    });

    $('.decrease-qty').click(function () {

        var target = $(this).data('target');
        var rowId = $(this).data('rowid');
        var qty = parseInt($('.' + target).val());
        if (qty > 1) {
            $('.' + target).val(qty - 1);
            updateCartQty(rowId, qty - 1);
        }
    });
    $('.increase-qty').click(function () {
        var target = $(this).data('target');
        var rowId = $(this).data('rowid');
        var qty = parseInt($('.' + target).val());
        if (qty < 10) {
            $('.' + target).val(qty + 1);
            updateCartQty(rowId, qty + 1);
        }
    });
    $('.remove-from-cart').click(function () {
        var rowId = $(this).data('rowid');
        removeItemFromCart(rowId);
    });

    $('.btn_filterpr').click(function () {
        console.log($('.startprice'));
    });

});
function updateCartNotiQty(qty) {
    if (qty > 0) {
        $('.cart-qty-notification').show().text(qty);
    } else {
        $('.cart-qty-notification').hide();
    }

}

function updateCartQty(rowId, qty) {
    $.ajax({
        url: BASE_URL + 'cart/update',
        type: 'post',
        dataType: 'json',
        data: {'rowid': rowId, 'qty': qty},
        success: function (res) {
            if (!res.error) {
                updateCartNotiQty(res.qty);
                updateCartTotal(rowId, res.total, res.subtotal);
            } else {
                console.log(res.error);
            }
        }
    });
}

function removeItemFromCart(rowId) {
    $.confirm({
        title: 'Xóa khỏi giỏ hàng',
        content: 'Bạn đồng ý loại bỏ sản phẩm này khỏi đơn hàng?',
        buttons: {
            close: {
                text: 'Hủy',
                function() {}
            },
            yes: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: BASE_URL + 'cart/update',
                        type: 'post',
                        dataType: 'json',
                        data: {'rowid': rowId, 'qty': 0},
                        success: function (res) {
                            if (!res.error) {
                                updateCartNotiQty(res.qty);
                                updateCartTotal(rowId, res.total, res.subtotal);
                                if (res.qty > 0) {
                                    $('tr#' + rowId).fadeOut();
                                } else {
                                    window.location.reload();
                                }
                            } else {
                                console.log(res.error);
                            }
                        }
                    });
                }
            }
        }
    });

}

function updateCartTotal(rowId, total, subtotal) {
    $('#subtotal_' + rowId).text(subtotal);
    $('#cart-index-subtotal, #cart-index-total').html(total);
}

function loadmorehome(el) {
    var page = $(el).data('page');
    $.ajax({
        url: BASE_URL + 'home/' + page,
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            $('#bind-home-products').append(res.products);
            $('.product-tile-wrapper').fadeIn('slow');
            if (!res.next) {
                $(el).hide();
            } else {
                $(el).data('page', res.next);
            }
        }
    });
}

function showHeaderPanel(panel) {
    $('#'+panel).addClass('open');
}

function hideHeaderPanel(panel) {
    $('#'+panel).removeClass('open');
}