<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'admin/dashboard';
// $route['admin/login'] = 'admin/login';

// Detail
$route['page/(:any)\.html'] = 'frontend/page/index/$1';
$route['home/(:num)'] = 'home/index/$1';
$route['search'] = 'frontend/search/index';
$route['search/page/(:num)'] = 'frontend/search/page/$1';
$route['product/addtocart'] = 'frontend/product/addtocart';
$route['product/findtobrand'] = 'frontend/product/findtobrand';
$route['product/findtoprice'] = 'frontend/product/findtoprice';
$route['search/filtertobrandsearch'] = 'frontend/search/filtertobrandsearch';
$route['thankyou'] = 'frontend/thankyou/index';
$route['cart'] = 'frontend/cart/index';
$route['cart/update'] = 'frontend/cart/update';
$route['checkout'] = 'frontend/cart/checkout';
$route['(:any)-(:num)-(:num)\.phtml'] = 'frontend/product/detail/$1/$2/$3';
$route['(:any)-(:num)\.phtml'] = 'frontend/product/detail/$1/$2';
$route['(:any)/page/(:num)'] = 'frontend/product/index/$1/$2';
$route['(:any)'] = 'frontend/product/index/$1';
$route['(:any)/filter'] = 'frontend/product/index_brand/$1';
$route['admin/sales/delete_details/(:num)-(:num)'] = 'admin/sales/delete_details/$1/$2';