<?php

/*
 * To change this license header, choose License Headers in Introduce Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Introduces_model
 *
 * @author TNM Group
 */
class Introduce_model extends CI_Model {
    private $table = 'introduce';

    public function __construct() {
        parent::__construct();
    }

    public function getIntroduces($page = 0) {  
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('sort','ASC');
        $query = $this->db->get($this->table);
        $introduces = $query->result();
        return ["total" => $total, "introduces" => $introduces];
    }
    

    public function getIntroduceById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row_array();
        } else {
            return NULL;
        }
    }

    public function insert() {
        $slug_vn = create_slug($this->input->post('title_vn'));
        $slug_en = create_slug($this->input->post('title_en'));
        $slug = serialize(["vn" => $slug_vn, "en" => $slug_en ]);
        $name = serialize(["vn" => $this->input->post('name_vn'), "en" => $this->input->post('name_en') ]);
        $title = serialize(["vn" => $this->input->post('title_vn'), "en" => $this->input->post('title_en') ]);
        $content = serialize(["vn" => $this->input->post('content_vn'), "en" => $this->input->post('content_en') ]);
        
        $data = array(
            'name' => $name,
            'title' => $title,
            'slug' => $slug,
            'content' => $content,
            'created_time' => date('Y-m-d H:i:s'),
        );
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update() {
        $slug_vn = create_slug($this->input->post('title_vn'));
        $slug_en = create_slug($this->input->post('title_en'));
        $slug = serialize(["vn" => $slug_vn, "en" => $slug_en ]);
        $name = serialize(["vn" => $this->input->post('name_vn'), "en" => $this->input->post('name_en') ]);
        $title = serialize(["vn" => $this->input->post('title_vn'), "en" => $this->input->post('title_en') ]);
        $content = serialize(["vn" => $this->input->post('content_vn'), "en" => $this->input->post('content_en') ]);
        
        $data = array(
            'name' => $name,
            'title' => $title,
            'slug' => $slug,
            'content' => $content,
            'updated_time' => date('Y-m-d H:i:s'),
        );

        $id = $this->input->post('pid');
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

}
