<?php

/*
 * To change this license header, choose License Headers in Gallery Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gallerys_model
 *
 * @author TNM Group
 */
class Gallery_model extends CI_Model {

    private $table = 'gallery';

    public function __construct() {
        parent::__construct();
    }

    public function getGallerys($page = 0) {
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ? 0 : ($page - 1) * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($this->table);
        $gallerys = $query->result();
        foreach ($gallerys as $key => $g) {
            $query = $this->db->get_where('gallery_images', ['gallery_id' => $g->id]);
            $images = $query->result();
            $gallerys[$key]->images = $images;
        }
//        custom_debug($gallerys);die();
        return ["total" => $total, "gallerys" => $gallerys];
    }

    public function getGalleryById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            $gallery = $query->row_array();
            $query = $this->db->get_where('gallery_images', array('gallery_id' => $id));
            $images = $query->result();
            $gallery['images'] = $images;
            return $gallery;
        } else {
            return NULL;
        }
    }

    public function insert() {
        //die('1');
        $title = serialize(["vn" => $this->input->post('title_vn'), "en" => $this->input->post('title_en')]);

        $data = array(
            'title' => $title,
            'created_time' => date('Y-m-d H:i:s'),
        );
        $this->db->insert($this->table, $data);
        $gallery_id = $this->db->insert_id();
        $uploaded = $this->handle_image_upload();
        foreach ($uploaded['uploads'] as $key => $v) {
            $file = array(
                'gallery_id' => $gallery_id,
                'filepath' => "media/gallery_image/" . $v['file_name']
            );
            $this->db->insert('gallery_images', $file);
        }
        return $this->db->insert_id();
    }

    public function update() {
        $title = serialize(["vn" => $this->input->post('title_vn'), "en" => $this->input->post('title_en')]);
        $data = array(
            'title' => $title,
            'updated_time' => date('Y-m-d H:i:s'),
        );
        $uploaded = $this->handle_image_upload();
        $id = $this->input->post('pid');
        foreach ($uploaded['uploads'] as $key => $v) {
            $file = array(
                'gallery_id' => $id,
                'filepath' => "media/gallery_image/" . $v['file_name']
            );
            $this->db->insert('gallery_images', $file);
        }        
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

    function handle_image_upload() {
        if ($this->input->post()) {
            // retrieve the number of images uploaded;
            $number_of_files = sizeof($_FILES['images']['tmp_name']);
            // considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
            $files = $_FILES['images'];
            $errors = array();

            // first make sure that there is no error in uploading the files
            for ($i = 0; $i < $number_of_files; $i++) {
                if ($_FILES['images']['error'][$i] != 0)
                    $errors[$i][] = 'Couldn\'t upload file ' . $_FILES['images']['name'][$i];
            }
            if (sizeof($errors) == 0) {
                // now, taking into account that there can be more than one file, for each file we will have to do the upload
                // we first load the upload library
                $config['encrypt_name'] = TRUE;
                $config['upload_path'] = './media/gallery_image/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                for ($i = 0; $i < $number_of_files; $i++) {
                    $_FILES['uploadedimage']['name'] = $files['name'][$i];
                    $_FILES['uploadedimage']['type'] = $files['type'][$i];
                    $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
                    $_FILES['uploadedimage']['error'] = $files['error'][$i];
                    $_FILES['uploadedimage']['size'] = $files['size'][$i];
                    //now we initialize the upload library
                    // we retrieve the number of files that were uploaded
                    if ($this->upload->do_upload('uploadedimage')) {
                        $data['uploads'][$i] = $this->upload->data();
                        $this->load->helper('image');
                        resize_image($data['uploads'][$i]['full_path'], 960, 560);
                    } else {
                        $data['upload_errors'][$i] = $this->upload->display_errors();
                    }
                }
                return $data;
            } else {
//                print_r($errors);
                return ['error' => TRUE];
            }
//            echo '<pre>';
//            print_r($data);
//            echo '</pre>';
        }
    }

}
