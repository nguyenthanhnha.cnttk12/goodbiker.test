<?php

/*
 * To change this license header, choose License Headers in Sponsor Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sponsors_model
 *
 * @author TNM Group
 */
class Sponsor_model extends CI_Model {

    private $table = 'sponsors';

    public function __construct() {
        parent::__construct();
    }

    public function getSponsors($page = 0) {
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ? 0 : ($page - 1) * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($this->table);
        $sponsors = $query->result();
        return ["total" => $total, "sponsors" => $sponsors];
    }

    public function getSponsorById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row_array();
        } else {
            return NULL;
        }
    }

    public function insert() {
        $feature_image = $this->input->post('feature_image');
        $name = $this->input->post('name');
        $link = $this->input->post('link');

        $data = array(
            'name' => $name,
            'link' => $link,
            'feature_image' => $feature_image,
            'created_time' => date('Y-m-d H:i:s'),
        );
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update() {
        $name = $this->input->post('name');
        $link = $this->input->post('link');

        $data = array(
            'name' => $name,
            'link' => $link,
            'updated_time' => date('Y-m-d H:i:s'),
        );
        //var_dump($_POST);die();
        if ($this->input->post('feature_image')) {
            $data['feature_image'] = $this->input->post('feature_image');
        }

        $id = $this->input->post('pid');
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

}
