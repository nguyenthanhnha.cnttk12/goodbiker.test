<?php

/*
 * To change this license header, choose License Headers in Order Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Orders_model
 *
 * @author TNM Group
 */
class Order_model extends CI_Model {

    private $table = 'orders';

    public function __construct() {
        parent::__construct();
    }

    public function getAllOrders() {
        $this->db->order_by('order_id', 'DESC');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getOrders($page = 0) {
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ? 0 : ($page - 1) * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('order_id', 'DESC');
        $this->db->from($this->table . ' o');
        $this->db->join('shipping s', 'o.order_id = s.order_id', 'left');
        $this->db->select('o.*, s.address');
        $query = $this->db->get();
        $orders = $query->result();
        foreach ($orders as $key => $order) {
            $this->db->select('SUM(price * qty) as total');
            $this->db->where('order_id', $order->order_id);
            $query = $this->db->get('order_detail');
            $order->total = $query->row()->total;
        }
        // debug_sql();
        return ["total" => $total, "orders" => $orders];
    }

    public function getOrderById($id = 0) {
        if ((int) $id > 0) {
            $this->db->where('o.order_id', $id);
            $this->db->from($this->table . ' o');
            $this->db->join('shipping s', 'o.order_id = s.order_id', 'left');
            $this->db->select('o.*, s.address');
            $query = $this->db->get();
            $order = $query->row();
            $this->db->where('o.order_id', $order->order_id);
            $this->db->from('order_detail o');
            $this->db->join('products p', 'o.product_id = p.id', 'left');
//            $this->db->select('o.*, p.name as product_name, p.id as product_id');            

            $this->db->group_by('p.id');
            $this->db->join('product_images im', 'p.id = im.product_id', 'left');
            $this->db->join('product_category c', 'p.id = c.product_id', 'left');
            $this->db->join('categories cat', 'c.category_id = cat.id', 'left');
            $this->db->where(array('im.featured' => 'Yes'));
//        $this->db->like('p.name ', $searchterm);
//        $this->db->or_like('cat.name ', $searchterm);
            $this->db->select('o.*, p.id as product_id, p.name as product_name, p.slug, im.path as image, c.category_id');
            $query = $this->db->get();
            $order->items = $query->result();
//            custom_debug($order);die();
            return $order;
        } else {
            return NULL;
        }
    }

    public function insert() {
        // custom_debug($_POST); die();
        $slug = (!empty($this->input->post('slug'))) ? $this->input->post('slug') : create_slug($this->input->post('name'));
        $name = $this->input->post('name');
        $brand_id = $this->input->post('brand_id');
        $description = $this->input->post('description');
        $price = $this->input->post('price');
        $now = date('Y-m-d H:i:s');

        $uniq_slug = createUniqueSlug($slug, 'orders', 'slug');

        $data = array(
            'name' => $name,
            'slug' => $uniq_slug,
            'brand_id' => $brand_id,
            'description' => $description,
            'price' => $price,
            'created_date' => $now
        );
        if ($this->input->post('is_active') == 'Yes') {
            $data['is_active'] = 'Yes';
        } else {
            $data['is_active'] = 'No';
        }
        if (!$this->db->insert($this->table, $data)) {
            return false;
        }
        $order_id = $this->db->insert_id();
        //Add Order images
        $image = $this->input->post('image');
        $other_imgs = $this->input->post('other_img');
        $order_images = array(['order_id' => $order_id, 'featured' => 'Yes', 'path' => $image]);
        foreach ($other_imgs as $key => $img) {
            $order_images[] = ['order_id' => $order_id,
                'path' => $img,
                'featured' => 'No'
            ];
        }
        $this->db->insert_batch('order_images', $order_images);

        //Add prouct - category
        $categories = $this->input->post('category_id');
        $order_category = [];
        foreach ($categories as $key => $category_id) {
            $order_category[] = ['order_id' => $order_id,
                'category_id' => $category_id
            ];
        }
        $this->db->insert_batch('order_category', $order_category);
        return $order_id;
    }

    public function update() {
        $id = $this->input->post('pid');

        $data = array(
            'status' => $this->input->post('status')
        );
        return $this->db->update($this->table, $data, array('order_id' => $id));
    }

    public function delete($id) {
        $this->db->delete('order_detail', ['order_id' => $id]);
        $this->db->delete('orders', ['order_id' => $id]);
    }

    

}
