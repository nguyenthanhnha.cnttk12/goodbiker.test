<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends CI_Model
{

    private $table_name = 'system_users';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        //$this->user_id = $this->session->userdata['user_info']['id'];
    }

    function add($arr)
    {
        $this->first_name = isset($arr['first_name']) ? $arr['first_name'] : "";
        $this->last_name = isset($arr['last_name']) ? $arr['last_name'] : "";
        $this->company_name = isset($arr['company_name']) ? $arr['last_name'] : "";
        $this->email = $arr['email'];
        $this->pwd = $arr['pwd'];
        $this->enable = $arr['enable'];
        $this->slug_general = $arr['slug_general'];
        $this->registration_date = date("Y-m-d H:i:s");
        $this->update_date = date("Y-m-d H:i:s");
        $this->db->insert($this->table_name, $this);
    }

    function update($arr)
    {
        $this->first_name = $arr['first_name'];
        $this->last_name = $arr['last_name'];
        $this->company_name = $arr['company_name'];
        if (!empty($arr['pwd']))
        {
            $this->pwd = $arr['pwd'];
        }
        $this->update_date = date("Y-m-d H:i:s");
        $this->db->update($this->table_name, $this, array('email' => $arr['email']));
    }

    function update_member($id, $arr)
    {
        $this->db->update($this->table_name, $arr, array('id' => $id));
    }

    function get_record_by_email($email)
    {
        $query = $this->db->get_where($this->table_name, array('email' => $email));
        return $query->row_array();
    }

    function get_record_by_slug($slug)
    {
        $query = $this->db->get_where($this->table_name, array('slug_general' => $slug));
        return $query->row_array();
    }

    function get_profile_by_id($id)
    {
        $query = $this->db->get_where($this->table_name, array('id' => $id));
        return $query->row_array();
    }

    public function get_about_info_by_id($user_id)
    {
        $this->db->trans_start();
        $query = $this->db->get_where($this->workplace_table, array('user_id' => $user_id));
        $workplace = $query->row_array();
        $query = $this->db->get_where($this->education_table, array('user_id' => $user_id));
        $education = $query->row_array();
        $query = $this->db->get_where($this->liveplace_table, array('user_id' => $user_id));
        $liveplace = $query->row_array();
        $query = $this->db->get_where($this->contact_table, array('user_id' => $user_id));
        $contact = $query->row_array();
        $this->db->trans_complete();
        $result = array('error' => false);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
            $result['workplace'] = $workplace;
            $result['education'] = $education;
            $result['liveplace'] = $liveplace;
            $result['contact'] = $contact;
        }
        return $result;
    }

    public function get_workplace($user_id)
    {
        $this->db->trans_start();
        $query = $this->db->get_where($this->workplace_table, array('user_id' => $user_id));
        $workplace = $query->row_array();
        $this->db->trans_complete();
        $result = array('error' => false);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
            $result['workplace'] = $workplace;
        }
        return $result;
    }

    public function get_education($user_id)
    {
        $this->db->trans_start();
        $query = $this->db->get_where($this->education_table, array('user_id' => $user_id));
        $education = $query->row_array();
        $this->db->trans_complete();
        $result = array('error' => false);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
            $result['education'] = $education;
        }
        return $result;
    }

    public function get_liveplace($user_id)
    {
        $this->db->trans_start();
        $query = $this->db->get_where($this->liveplace_table, array('user_id' => $user_id));
        $liveplace = $query->row_array();
        $this->db->trans_complete();
        $result = array('error' => false);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
            $result['liveplace'] = $liveplace;
        }
        return $result;
    }

    public function get_contact_info($user_id)
    {
        $this->db->trans_start();
        $query = $this->db->get_where($this->contact_table, array('user_id' => $user_id));
        $contact = $query->row_array();
        $this->db->trans_complete();
        $result = array('error' => false);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
            $result['contact'] = $contact;
        }
        return $result;
    }

    /**
     * Workplace
     */

    public function insert_workplace($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->workplace_table, $data);
        $this->db->trans_complete();
        $result = array('error' => false);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
        }
        return $result;
    }

    public function update_workplace($data, $id)
    {
        $this->db->trans_start();
        $this->db->update($this->workplace_table, $data, array('id' => $id));
        $this->db->trans_complete();
        $result = array('error' => 0);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
        }
        return $result;
    }

    /**
     * Education
     */

    public function insert_education($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->education_table, $data);
        $this->db->trans_complete();
        $result = array('error' => false);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
        }
        return $result;
    }

    public function update_education($data, $id)
    {
        $this->db->trans_start();
        $this->db->update($this->education_table, $data, array('id' => $id));
        $this->db->trans_complete();
        $result = array('error' => 0);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
        }
        return $result;
    }

    /**
     * Live place
     */

    public function insert_liveplace($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->liveplace_table, $data);
        $this->db->trans_complete();
        $result = array('error' => false);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
        }
        return $result;
    }

    public function update_liveplace($data, $id)
    {
        $this->db->trans_start();
        $this->db->update($this->liveplace_table, $data, array('id' => $id));
        $this->db->trans_complete();
        $result = array('error' => 0);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
        }
        return $result;
    }

    /**
     * Contact
     */

    public function insert_contact($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->contact_table, $data);
        $this->db->trans_complete();
        $result = array('error' => false);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
        }
        return $result;
    }

    public function update_contact($data, $id)
    {
        $this->db->trans_start();
        $this->db->update($this->contact_table, $data, array('id' => $id));
        $this->db->trans_complete();
        $result = array('error' => 0);
        if ($this->db->trans_status() === false)
        {
            // generate an error... or use the log_message() function to log your error
            $result['error'] = true;
        } else
        {
            $result['error'] = false;
        }
        return $result;
    }

    public function search_members($name, $friend = false, $notIn = array())
    {
        $this->db->select('M.id, M.first_name, M.last_name, A.file_path');
        $this->db->from($this->table_name . ' AS M ');
        $this->db->join($this->avatar_table . ' AS A ', 'A.user_id = M.id', 'left');
        //$this->db->like('M.first_name',$name, 'after');
        //$this->db->or_like('M.last_name',$name, 'after');
        $this->db->where("( M.first_name like '$name" . "%' OR M.last_name like '$name" .
            "%' OR CONCAT(M.first_name,' ',M.last_name) like '%" . $name . "%' )");
        if((int)$this->user_id > 0) 
            $this->db->where('M.id !=', $this->user_id);
        if (count($notIn) > 0)
        {
            $this->db->where_not_in('M.id', $notIn);
        }
        $friend_ids = array();
        if ($friend)
        {
            $curr_user_id = $this->user_id;
            //Get friends list
            $query = $this->db->query("SELECT * FROM `relationship`
                                        WHERE (`user_one_id` = '$curr_user_id' OR `user_two_id` = '$curr_user_id')
                                        AND `status` = 1");
            $rs = $query->result_array();

            foreach ($rs as $key => $row)
            {
                if (!in_array($friend_ids, $row['user_one_id']))
                {
                    array_push($friend_ids, $row['user_one_id']);
                }
                if (!in_array($friend_ids, $row['user_two_id']))
                {
                    array_push($friend_ids, $row['user_two_id']);
                }
            }
        }
        if (count($friend_ids) > 0)
        {
            $this->db->where_in('M.id', $friend_ids);
        }
        $this->db->limit($this->config->item('search_results_per_page'));
        $query = $this->db->get();

        $result = $query->result_array();
        //die($this->db->last_query());
        return $result;
    }

    public function totalrecord($name)
    {
        $user_id = $this->user_id;
        $this->db->from($this->table_name);
        $this->db->where('id !=', $user_id);
        $this->db->like('first_name', $name, 'after');
        $this->db->or_like('last_name', $name, 'after');
        return $this->db->count_all_results();
    }

    public function getcollection($page = 0, $limit = 10, $name)
    {
        $page = ($page > 0) ? $page : 1;
        $offset = ($page - 1) * $limit;
        //$curr_user_id = $this->session->userdata['user_info']['id'];
        //$this->db->order_by('created_date', 'DESC');
        $this->db->select('M.id, M.first_name, M.last_name, A.file_path');
        $this->db->from($this->table_name . ' AS M ');
        $this->db->join($this->avatar_table . ' AS A ', 'A.user_id = M.id', 'left');
        $this->db->like('M.first_name', $name, 'after');
        $this->db->or_like('M.last_name', $name, 'after');
        $this->db->where('M.id !=', $this->user_id);
        $query = $this->db->get();

        $rows = $query->result_array();
        $result = array();
        foreach ($rows as $key => $row)
        {
            $user_id = $row['id'];
            $arr = array();
            if ($user_id < $this->user_id)
            {
                $arr['user_one_id'] = $user_id;
                $arr['user_two_id'] = $this->user_id;
            } else
            {
                $arr['user_one_id'] = $this->user_id;
                $arr['user_two_id'] = $user_id;
            }
            $CI = &get_instance();
            $CI->load->model('relationship_model');

            $relationship = $CI->relationship_model->get_relationship_visitor($arr);
            $result[$key] = $row;
            $result[$key]['relationship'] = $relationship;
        }
        //print_r_pre($result);die();
        return $result;
    }

    public function getAvatar($user_id)
    {
        $query = $this->db->get_where($this->avatar_table, array('user_id' => $user_id));
        return $query->row_array();
    }

    function getmyprofile($member_id)
    {
        $this->db->select("*");
        $this->db->from("members as m");
        $this->db->join("member_contact as mc", "m.id=mc.user_id", "left");
        $this->db->where('m.id', $member_id);
        return $this->db->get()->row_array();
    }
    
    public function getRemindUsers() {
        $today = date("Y-m-d 00:00:00");
        $this->db->select('writer_id');
        $this->db->from($this->jot_table);        
        $this->db->where("created_date >= '".$today."'");
        $this->db->group_by("writer_id");
        $result = $this->db->get()->result_array();
        $not_remind = array();
        if(count($result) > 0) {
            foreach($result as $key => $row) {
                array_push($not_remind, $row['writer_id']);
            } 
            $this->db->where_not_in('id', $not_remind);           
        }
        $this->db->select('id, first_name, last_name, email');
        $this->db->from($this->table_name);
        return $this->db->get()->result_array();
    }
    public function get_member_id($arr){
        $this->db->trans_start();
        $this->db->select('id,first_name,last_name');
        $this->db->where_in('id', $arr);
        $query = $this->db->get($this->table_name);
        $this->db->trans_complete();
        return $query->result_array();
    }


}

/* Location: ./application/models/member_model.php */
