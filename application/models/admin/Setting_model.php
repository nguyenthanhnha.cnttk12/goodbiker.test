<?php

/*
 * To change this license header, choose License Headers in Setting Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings_model
 *
 * @author TNM Group
 */
class Setting_model extends CI_Model {

    private $table = 'site_settings';

    public function __construct() {
        parent::__construct();
    }

    public function getSettings() {
        $this->db->limit(1);
        $query = $this->db->get($this->table);
        $settings = $query->row();
        // custom_debug($settings);die();
        return $settings;
    }

    public function getSettingById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row_array();
        } else {
            return NULL;
        }
    }

    public function update() {
        // custom_debug($_POST);die();
        $logo = $this->input->post('logo');
        $seo_title = $this->input->post('seo_title');
        $keyword = $this->input->post('keyword');
        $seo_description = $this->input->post('seo_description');
        $seo = json_encode(['title' => $seo_title,'keyword' => $keyword, 'description' => $seo_description]);
        $hotline = $this->input->post('hotline');
        $support_email = $this->input->post('support_email');
        $facebook = $this->input->post('facebook');
        //$google_plus = $this->input->post('google_plus');
        //$youtube = $this->input->post('youtube');
        $social_link = json_encode(['facebook' => $facebook]);
        $contact_info = json_encode(['support_email' => $support_email,'description' => $this->input->post('description')]);

        $data = array(
            'logo' => $logo,
            'seo' => $seo,
            'contact_info' => $contact_info,
            'hotline' => $hotline,
            'social_link' => $social_link,
            'open_time' => '',
            'company_name' => '',
        );

        $this->db->limit(1);
        $query = $this->db->get($this->table);
        if($query->row()) {
            $row = $query->row();
            $id = $row->id;
            $this->db->update($this->table, $data, ['id' => $id]);
        } else {
            $this->db->insert($this->table, $data);
        }
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

}
