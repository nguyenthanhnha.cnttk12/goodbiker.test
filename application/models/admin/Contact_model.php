<?php

/*
 * To change this license header, choose License Headers in Contact Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contacts_model
 *
 * @author TNM Group
 */
class Contact_model extends CI_Model {
    private $table = 'contact_message';

    public function __construct() {
        parent::__construct();
    }

    public function getContacts($page = 0) {  
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id','DESC');
        $query = $this->db->get($this->table);
        $contacts = $query->result();
        //echo $this->db->last_query();
        return ["total" => $total, "contacts" => $contacts];
    }
    

    public function getContactById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row_array();
        } else {
            return NULL;
        }
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

}
