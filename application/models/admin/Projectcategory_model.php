<?php

/*
 * To change this license header, choose License Headers in Projectcategory Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Projectcategorys_model
 *
 * @author TNM Group
 */
class Projectcategory_model extends CI_Model {
    private $table = 'project_categories';

    public function __construct() {
        parent::__construct();
    }

    public function getCategories($page = 0) {  
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id','DESC');
        $query = $this->db->get($this->table);
        $categories = $query->result();
        return ["total" => $total, "categories" => $categories];
    }
    

    public function getProjectcategoryById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row_array();
        } else {
            return NULL;
        }
    }

    public function insert() {
        $slug_vn = create_slug($this->input->post('name_vn'));
        $slug_en = create_slug($this->input->post('name_en'));
        $slug = serialize(["vn" => $slug_vn, "en" => $slug_en ]);
        $name = serialize(["vn" => $this->input->post('name_vn'), "en" => $this->input->post('name_en') ]);
        
        $data = array(
            'name' => $name,
            'slug' => $slug,
        );
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update() {
        $slug_vn = create_slug($this->input->post('name_vn'));
        $slug_en = create_slug($this->input->post('name_en'));
        $slug = serialize(["vn" => $slug_vn, "en" => $slug_en ]);
        $name = serialize(["vn" => $this->input->post('name_vn'), "en" => $this->input->post('name_en') ]);
        
        $data = array(
            'name' => $name,
            'slug' => $slug,
        );

        $id = $this->input->post('pid');
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

}
