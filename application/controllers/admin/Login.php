<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'html', 'file', 'path', 'secure'));
        //$this->load->library('form_validation');
        //$this->load->model('pages_model');
    }

    public function index() {
        if ($this->session->userdata('is_admin')) {
            redirect(site_url('admin/'));
        }
        $data = [];
        if ($this->input->post('email')) {
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            //$pass = password_verify();
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() !== false) {
                $this->load->model('admin/users_model');
                $record = $this->users_model->get_record_by_email($email);
                //var_dump($record);die();
                if ($record == null || empty($record)) {
                    $data['message'] = 'Email hay mật khẩu không đúng.';
                } else {
                    //die($record["pwd"]."---".$_POST["email"] . ":" . $_POST["password"]);
                    if (password_verify($pass, $record["password"])) {
                        //die("ok21");
                        $this->session->set_userdata('is_admin', true);
                        $this->session->set_userdata('user_info', array(
                            'email' => $record["email"],
                            'id' => $record["id"],
                            'name' => $record["name"]));
                        redirect("/admin");
                    } else {
                        //die("ok3");
                        $data['message'] = 'Sai mật khẩu.';
                    }
                    //die("ok4");
                }
                //die("ok5");
            } else {
                $data['message'] = validation_errors();
            }
        }
        $this->load->view('admin/login', $data);
    }

    public function signout() {
        $this->session->unset_userdata("is_admin");
        $this->session->unset_userdata("user_info");
        redirect("/admin");
    }

}
