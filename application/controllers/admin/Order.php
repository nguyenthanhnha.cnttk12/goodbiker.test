<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Order extends MY_Controller {
    
    // public $menu = 'order';
    // public $page_title = 'Đơn hàng';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'order';
        $this->page_title = 'Đơn hàng';
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/order_model');
        $this->_getCategories();
    }

    public function index() {

        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->order_model->getOrders($page);
        //var_dump($data);die();
        $this->_renderAdminLayout('admin/order/index', $data);
    }

    public function add() {
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        if ($this->input->post('save')) {
            //var_dump($_POST);die();
            $this->_save();
        } else {
            $data['category_lineage'] = $this->category_lineage;
            // var_dump($this->category_lineage);die();
            $data['controller'] = $this;
            $this->_renderAdminLayout('admin/order/add', $data);
        }
    }

    public function edit($id) {
        $data['order'] = $this->order_model->getOrderById($id);
        $data['controller'] = $this;
        $data['pid'] = $this->input->post('pid'); // $id;
        if ($this->input->post('save')) {
            $this->_save();
        } else {
            $this->_renderAdminLayout('admin/order/edit', $data);
        }
    }

    public function _save() {
        $data = $this->input->post();

        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {
            $this->order_model->update();
            $this->session->set_flashdata('msg', 'Đơn hàng được cập nhật thành công!');
            redirect('/admin/order/edit/' . $id);
        } else {
            //var_dump($data);die();
            $id = $this->order_model->insert();
            $this->session->set_flashdata('msg', 'Đơn hàng mới đã được thêm vào dữ liệu!');
            redirect('/admin/order/');
        }
    }

    public function delete($id) {
        if ((int) $id > 0) {
            $this->order_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'Đơn hàng đã được xóa!');
        redirect('/admin/order');
    }

    public function action() {
        $val = $this->input->post('val');
        $action = $this->input->post('hidAction');
        if ($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('orders');
        }
        if ($action == 'sorting') {
            $sort = $this->input->post('sort');
            foreach ($val as $key => $value) {
                $this->db->update('orders', ["sort" => $sort[$key]], ["id" => $value]);
            }
        }
        redirect('/admin/order');
    }

    public function _getAllorders() {
        return $this->order_model->getAllorders();
    }

}
