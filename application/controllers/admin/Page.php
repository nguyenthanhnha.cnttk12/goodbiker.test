<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Page extends MY_Controller {
    
    // public $menu = 'page';
    // public $page_title = 'Sản phẩm';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'page';
        $this->page_title = 'Hướng dẫn';
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/page_model');
    }

    public function index() {

        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->page_model->getPages($page);
        //var_dump($data);die();
        $this->_renderAdminLayout('admin/page/index', $data);
    }

    public function add() {
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        if ($this->input->post('save')) {
            //var_dump($_POST);die();
            $this->_save();
        } else {
            $data['controller'] = $this;
            $this->_renderAdminLayout('admin/page/add', $data);
        }
    }

    public function edit($id) {
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        $data['page'] = $this->page_model->getPageById($id);
        $data['controller'] = $this;
        $data['pid'] = $this->input->post('pid'); // $id;
        if ($this->input->post('save')) {
            $this->_save();
        } else {
            $this->_renderAdminLayout('admin/page/edit', $data);
        }
    }

    public function _save() {
        $data = $this->input->post();
//        custom_debug($data);die();
        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {
            $this->page_model->update();
            $this->session->set_flashdata('msg', 'Cập nhật thành công!');
            redirect('/admin/page/edit/' . $id);
        } else {
            //var_dump($data);die();
            $id = $this->page_model->insert();
            $this->session->set_flashdata('msg', 'Trang mới đã được thêm vào dữ liệu!');
            redirect('/admin/page/');
        }
    }

    public function delete($id) {
        if ((int) $id > 0) {
            $this->page_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'Trang đã được xóa!');
        redirect('/admin/page');
    }

    public function action() {
        $val = $this->input->post('val');
        $action = $this->input->post('hidAction');
        if ($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('pages');
        }
        if ($action == 'sorting') {
            $sort = $this->input->post('sort');
            foreach ($val as $key => $value) {
                $this->db->update('pages', ["sort" => $sort[$key]], ["id" => $value]);
            }
        }
        redirect('/admin/page');
    }

    public function _getAllpages() {
        return $this->page_model->getAllpages();
    }

}
