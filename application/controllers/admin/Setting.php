<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Setting extends MY_Controller {
    
    public $menu = 'setting';

    public function __construct() {
        parent::__construct();
        is_admin();
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/setting_model');
    }

    public function index() {
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        $data['settings'] = $this->setting_model->getSettings();
        $this->_renderAdminLayout('admin/setting/index', $data);
    }

    public function save() {
        $this->setting_model->update();
        $this->session->set_flashdata('msg', 'Cập nhật thành công!');
        redirect('/admin/setting');
    }

}
