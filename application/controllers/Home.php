<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_title = 'GoodBikerVN';
        $this->load->model('admin/product_model');
        $this->load->model('admin/category_model');
    }

    public function index($page = 1) {
//        die('home');
        $this->canonical_url = site_url();
        $this->page_seo->type = 'website';
        $this->page_seo->meta_url = $this->canonical_url;
        $data = $this->_getProducts($page);
        $this->_renderFrontLayout('home/index', $data);
    }

    public function _getProducts($page) {
//        var_dump($this->category_lineage);
        $item_per_page = 30;
        $result = $this->product_model->getFrontProducts($page, $item_per_page);
        $pagecount = ceil($result['total'] / $item_per_page);
        $page = $page + 1;
        $next = ($page > $pagecount) ? FALSE : $page;
        if ($this->input->is_ajax_request()) {
            $result['isajax'] = TRUE;
            $output = $this->_loadElement('home/list', $result, TRUE);
            die(json_encode(["next" => $next, "products" => $output]));
        }
        $result['isajax'] = FALSE;
        $result['next'] = $next;
        return $result;
    }

}
