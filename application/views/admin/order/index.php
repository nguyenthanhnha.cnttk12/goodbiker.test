<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Đơn hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Đơn hàng</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
            <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                <strong>Success! </strong>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <form id="frmMain" method="POST" action="/admin/order/action">
                        <div class="box-header">
                            <h3 class="box-title">&nbsp;</h3>

                            <div class="box-tools">
                                <div class="btn-group pull-right">
                                    <!--<a class="btn btn-sm btn-primary " href="<?php echo site_url('/admin/order/add') ?>"><i class="fa fa-plus"></i> Thêm mới</a>-->
                                    <a id="bulk-delete" class="btn btn-sm btn-danger " data-toggle="confirmation" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i> Xóa</a>
                                    <input type="hidden" id="hidAction" name="hidAction" value="" />
                                </div>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-bordered table-hover table-striped">
                                <tr>
                                    <th style="width: 20px"><input type="checkbox" class="minimal checkth" ></th>
                                    <th>Mã Đơn hàng</th>
                                    <th>Khách hàng</th>
                                    <th>Ngày đặt hàng</th>
                                    <th>Tổng cộng</th>
                                    <th>Thanh toán</th>
                                    <th>Trạng thái</th>
                                    <th style="width: 80px"></th>
                                </tr>
                                <?php
                                $status = ['pending' => 'Chờ xử lý', 'confirmed' => 'Đã xác nhận', 'completed' => 'Đã hoàn thành', 'cancel' => 'Đã hủy'];
                                ?>
                                <?php if ($data['total'] > 0) { ?>
                                    <?php foreach ($data['orders'] as $key => $order) { ?>
                                        <tr>
                                            <td><input type="checkbox" class="minimal checkitem" name="val[]" value="<?php echo $order->order_id ?>" ></td>                                            
                                            <td><?php echo $order->order_code ?></td>
                                            <?php
                                            $address = unserialize($order->address);
                                            ?>
                                            <td>
                                                <ul class="list-unstyled">
                                                    <li><?php echo $address['name'] ?></li>
                                                    <li>ĐT: <?php echo $address['phone'] ?></li>
                                                    <li>ĐC: <?php echo $address['address'] ?></li>
                                                </ul>
                                                
                                            </td>
                                            <td><?php echo date('d/m/Y', strtotime($order->order_date));?></td>
                                            <td><?php echo number_format($order->total, 0, ',', '.'); ?> ₫</td>
                                            <td><?php echo strtoupper($order->payment_method) ;?></td>
                                            <td><?php echo $status[$order->status] ;?></td>
                                            <td>
                                                <a href="/admin/order/edit/<?php echo $order->order_id; ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
                                                <a href="/admin/order/delete/<?php echo $order->order_id; ?>" class="btn btn-xs btn-danger" data-toggle="confirmation" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                        <tr>
                                            <td colspan="3" class="text-center"><?php echo $this->config->item('no_data')?></td>
                                        </tr>
                                <?php }?>
                            </table>

                        </div>
                        <div class="box-footer clearfix">
                            <?php echo custom_pagination('/admin/order/index/', $data['total']);?>
                        </div>
                    </form>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                
            </div>
        </div>
    </section>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->