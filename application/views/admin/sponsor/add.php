<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Đối tác
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Đối tác</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm mới</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                    <div class="form-group">
                        <?php echo form_label('Logo', 'feature_image'); ?>
                        <?php echo form_error('feature_image', '<em class="error">', '</em>'); ?> 
                        <input type="file" class="form-control filestyle" data-buttonName="btn-primary" name="feature_image" id="feature_image" value="" data-buttonBefore="true"> 
                    </div>
                    <div class="form-group">
                        <img id="thumbPreview" src="" style="width: 200px;height: auto;" />
                    </div>
                    <div class="form-group">
                        <label for="title_vn">Công ty</label>
                        <input type="text" class="form-control" required="" id="name" name="name" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input type="text" class="form-control" required="" id="link" name="link" placeholder="">
                    </div>
                </div>


                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1">Lưu</button>
                    <a href="<?php echo site_url('admin/sponsor') ?>" class="btn btn-default">Hủy</a>
                    <input type="hidden" name="pid" value="0" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
