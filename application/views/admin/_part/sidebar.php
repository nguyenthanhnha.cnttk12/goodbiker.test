<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="<?php echo site_url('assets/adminlte/') ?>/dist/img/avatar5.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Quản trị viên</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php echo active_class($menu, ['dashboard']) ?>">
          <a href="/admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>

        </li>

        <li class="<?php echo active_class($menu, ['category']) ?>">
          <a href="/admin/category">
            <i class="fa fa-th"></i> <span>Danh mục</span>
          </a>
        </li>
        <li class="<?php echo active_class($menu, ['brand']) ?>">
          <a href="/admin/brand">
            <i class="fa fa-cube"></i> <span>Hãng</span>
          </a>
        </li>

        <li class="<?php echo active_class($menu, ['product']) ?>">
          <a href="/admin/product">
            <i class="fa fa-th"></i> <span>Sản phẩm</span>
          </a>
        </li>
        <li class="<?php echo active_class($menu, ['sales']) ?>">
          <a href="/admin/sales">
            <i class="fa fa-gift"></i> <span>Khuyến mãi</span>
          </a>
        </li>
        <li class="<?php echo active_class($menu, ['page']) ?>">
          <a href="/admin/page">
            <i class="fa fa-edit"></i> <span>Trang</span>
          </a>
        </li>

        <li class="<?php echo active_class($menu, ['slide']) ?>">
          <a href="/admin/slide">
            <i class="fa fa-film"></i> <span>Slider</span>
          </a>
        </li>

        <li class="<?php echo active_class($menu, ['contact']) ?>">
          <a href="/admin/order">
            <i class="fa fa-shopping-cart"></i> <span>Đơn hàng</span>
          </a>
        </li>

        <li class="treeview <?php echo active_class($menu, ['setting']) ?>">
          <a href="#">
            <i class="fa fa-cogs"></i> <span>Cấu hình</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo active_class($menu, ['setting']) ?>"><a href="/admin/setting"><i class="fa fa-cog"></i> Thiết lập chung</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
