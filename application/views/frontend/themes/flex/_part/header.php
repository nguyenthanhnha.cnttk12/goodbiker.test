<div id="sp-header-nav" class="sp-header-panel visible-sm visible-xs sp-header">
    <div class="sp-header-search-header text-right">
        <div class="col-xs-2 col-sm-2 text-center pull-right" style="background-color: #000;height: 60px;line-height: 60px;"><a class="btn " onclick="hideHeaderPanel('sp-header-nav')" style="font-size: 17px;"><i class="fa fa-times"></i></a></div>
    </div>
    <div style="padding: 20px;">
        <?php load_element($this->theme_path . '_part/sidebar')?>
    </div>
</div>
<div id="sp-header-search" class="sp-header-panel visible-sm visible-xs sp-header">
    <div class="sp-header-search-header text-right">
        <div class="col-xs-2 col-sm-2 text-center pull-right" style="background-color: #000;height: 60px;line-height: 60px;"><a class="btn " onclick="hideHeaderPanel('sp-header-search')" style="font-size: 17px;"><i class="fa fa-times"></i></a></div>
    </div>
    <div style="padding: 20px;">
        <form method="get" data-form-name="Site Search" class="" action="/search" accept-charset="UTF-8">
            <div class="input-group" id="adv-search">
                <input type="text" name="spm" class="form-control header-search-text" placeholder="Tìm kiếm trên Good Biker VN" />
                <div class="input-group-btn">
                    <div class="btn-group" role="group">
                        <button type="submit" class="btn "><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<section id="header" class="visible-md visible-lg stick">
    <div class="header-menu">
        <div class="container">
            <ul class="header-menu-links  header-menu-links--left list-unstyled">
                <li class="header-menu-item">
                    <a class="header-menu-link" href="/lien-he">
                        Hotline: <span class="header-menu-link-tel"><?php echo $hotline ?></span>
                    </a>
                </li>
            </ul>
            <!-- <ul class="header-menu-links  header-menu-links--right list-unstyled">
                <li class="header-menu-item">
                                    <a href="/auth/identity" class="header-menu-link  header-menu-link--account">Log In/Sign Up</a>            </li>
                                              <li class="header-menu-item">
                                    <a href="/account/orders" class="header-menu-link">Order History</a>          </li>
                                              <li class="header-menu-item">

              </li>
            </ul> -->
        </div>
    </div>
    <div id="sticky-header">
        <div class="header-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 header-logo">
                        <a class="header-logo-link" href="/" title="Good Biker VN"><img width="80px" src="<?php echo get_image_url($logo); ?>"></a>
                        <span class="hotline">
                            <i class="fa fa-phone"></i> <span class=""><?php echo $hotline ?></span>
                        </span>
                    </div>
                    <div class="col-md-8 header-search">
                        <div class="header-search-form">
                            <form method="get" data-form-name="Site Search" class="header__search-form" action="/search" accept-charset="UTF-8">
                                <div class="input-group" id="adv-search">
                                    <input type="text" name="spm" class="form-control header-search-text" placeholder="Tìm kiếm trên Good Biker VN" />
                                    <div class="input-group-btn">
                                        <div class="btn-group" role="group">

                                            <button type="submit" class="btn btn-primary header-search-btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="col-md-2 header-shop">
                        <a class="btn-shopping-cart" href="<?php echo site_url('cart'); ?>">
                            <i class="fa fa-shopping-cart"></i>
                            <span <?php echo ($this->cart->total_items() > 0) ? "style= 'display: block;'" : "style= 'display: none;'" ?> id="cart-qty-notification" class="cart-qty-notification"><?php echo $this->cart->total_items() ?></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-navigation hidden-sm hidden-xs">
            <nav class="header-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="nav">
                                <?php
$c = new stdClass;
$c->listing = '';

foreach ($category_lineage as &$c->category) {
	if ($c->category->parent_id == 0) {
		$active = $current_category && ($c->category->id == $current_category->id || $c->category->id == $current_category->parent_id) ? 'active' : '';
		$c->listing .= '<li class="nav-menu level1 fly ' . $active . '">' . recursive_ul($c->category, $current_category) . '</li>';
	}
}
?>
                                <ul class="list-unstyled good-nav">
                                    <?php
echo $c->listing;
?>
                                    <li class="nav-menu level1"><a href="<?php echo site_url('page/huong-dan-mua-hang.html') ?>" class="nav-link">Hướng dẫn mua hàng</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</section>
<section class="visible-sm visible-xs sp-header">
    <div class="row" style="background-color: #000;height: 60px;line-height: 60px;">
        <div class="col-xs-2 col-sm-2 text-center"><a class="btn " onclick="showHeaderPanel('sp-header-nav')" style=""><i class="fa fa-bars"></i></a></div>
        <div class="col-xs-2 col-sm-2 text-center"><a class="btn " onclick="showHeaderPanel('sp-header-search')" style=""><i class="fa fa-search"></i></a></div>
        <div class="col-xs-4 col-sm-4 text-center"><a class="" href="/" title="Good Biker VN"><img src="<?php echo site_url('assets/images/android-icon-48x48.png'); ?>"></a></div>
        <div class="col-xs-2 col-sm-2 text-center"><a class="btn " href="tel:+84935292146"><i class="fa fa-phone"></i></a></div>
        <div class="col-xs-2 col-sm-2 text-center">
            <a class="btn " href="<?php echo site_url('cart'); ?>" style="position: relative;"><i class="fa fa-shopping-cart"></i>
            <span <?php echo ($this->cart->total_items() > 0) ? "style= 'display: block;'" : "style= 'display: none;'" ?> id="cart-qty-notification" class="cart-qty-notification"><?php echo $this->cart->total_items() ?></span>
            </a>
        </div>
    </div>
</section>

