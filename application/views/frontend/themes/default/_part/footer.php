<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h4 class="footer-heading">Giới thiệu cửa hàng</h3>
                    <p>Chuyên bán các loại mũ bảo hiểm cao cấp, lên tem chất. Ngoài ra cửa hàng còn có các dịch vụ cho thuê lều trại, loa kéo,... GoodbikerVN cam kết mang đến cho quý khách sản phẩm chất lượng cùng giá tốt. </p>
            </div>
            <div class="col-md-3">
                <h4 class="footer-heading">Chăm sóc khách hàng</h3>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo site_url('page/huong-dan-mua-hang.html'); ?>">Hướng dẫn mua hàng</a></li>
                        <li><a href="<?php echo site_url('lien-he.html'); ?>">Liên hệ</a></li>
                    </ul>
            </div>
            <div class="col-md-6">
                <h4 class="footer-heading">Địa chỉ cửa hàng</h3>

                    <div class="map-responsive">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3826.157726446867!2d107.59565638872839!3d16.467549253506846!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a13d068f5e59%3A0x1870f800dd454081!2zNiBUcuG6p24gUXVhbmcgS2jhuqNpLCBQaMO6IEjhu5lpLCBUaMOgbmggcGjhu5EgSHXhur8sIFRo4burYSBUaGnDqm4gSHXhur8sIFZpZXRuYW0!5e0!3m2!1sen!2sus!4v1524497588352" width="100%" height="240" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-6">
                &copy; 2018 - GoodBikerVN
            </div>
            <div class="col-md-6 text-right">
                Design by FitDNN
            </div>
        </div>
    </div>
</section>