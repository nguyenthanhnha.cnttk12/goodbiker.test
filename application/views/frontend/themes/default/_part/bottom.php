<div id="go-top" style="display: none;"><i class="fa fa-angle-up fa-2x"></i></div>
<script type="text/javascript">var BASE_URL = '<?php echo base_url(); ?>'</script>
<?php $this->carabiner->display('js');?>
<script charset="UTF-8" src="<?php echo site_url(); ?>assets/js/float-panel.js" type="text/javascript"></script>
<script charset="UTF-8" src="<?php echo site_url(); ?>assets/js/frontend.js" type="text/javascript"></script>
<script charset="UTF-8" src="<?php echo site_url(); ?>assets/js/ga.js" type="text/javascript"></script>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript">
    $(document).ready(function () {
        function GetURLParameter(sParam) {
            var sPageURL = window.location.href;
            var url = sPageURL.split('?');
            if (url[1] != null) {
                var queryURL = url[1];
                var sURLVariables = queryURL.split('&');
                for (var i = 0; i < sURLVariables.length; i++) {
                    var sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] == sParam) {
                        if (sParameterName[1] != "") {
                            return "-" + sParameterName[1];
                        } else {
                            return sParameterName[1];
                        }
                    }
                }
                return "";
            } else
                return "";
        }
        function GetURLParameter2(sParam) {
            var sPageURL = window.location.href;
            var url = sPageURL.split('?');
            if (url[1] != null) {
                var queryURL = url[1];
                var sURLVariables = queryURL.split('&');
                for (var i = 0; i < sURLVariables.length; i++) {
                    var sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] == sParam) {
                        return sParameterName[1];
                    }
                }
                return "";
            } else
                return "";
        }
        function FilterByBrand(id,slug) {
             $('.loader').show();
                $.ajax({
                    type: "POST",
                    url: BASE_URL + "product/findtobrand",
                    dataType: 'json',
                    data: {'id': id, 'slug': slug},
                    success: function (res) {
                        // empty(.a21);
                        if (res != null) {
                            $('.loader').hide();
                            $('.products-list-content').html(res.html);
                        } else
                            $('.products-list-content').html('<h2>error</h2>');
                            jQuery('html, body').animate({
                                scrollTop: 0
                            }, '2000', 'swing');
                    }
                });

        }
        function FilterByPrice(Sprice,Eprice,id,slug) {

            $('.loader').show();
            $.ajax({
                type: "POST",
                url: BASE_URL + "product/findtoprice",
                dataType: 'json',
                data: {'Sprice': Sprice, 'Eprice': Eprice, 'id': id, 'slug': slug},
                success: function (res) {
                     jQuery('html, body').animate({
                                scrollTop: 0
                            }, '2000', 'swing');
                    // empty(.a21);
                    if (res != null) {
                        $('.loader').hide();

                        $('.products-list-content').html(res.html);
                    } else
                        $('.products-list-content').html('asdasd');
                }
            });
        }
        function SearchByBrand(id,spm) {
                Sprice = null;
                Eprice = null;
                $('.loader').show();
                console.log(spm);
                $.ajax({
                    type: "POST",
                    url: BASE_URL + "search/filtertobrandsearch",
                    dataType: 'json',
                    data: {'id': id, 'spm': spm,'Sprice':Sprice,'Eprice':Eprice},
                    success: function (res) {
                        jQuery('html, body').animate({
                                scrollTop: 0
                            }, '2000', 'swing');
                        // empty(.a21);
                        if (res != null) {
                            $('.loader').hide();
                            $('.products-list-content').html(res.html);
                        } else
                            $('.products-list-content').html('<h2>error</h2>');
                    }
                });
        }
        function SearchByPrice(Sprice,Eprice,id,spm) {

            $('.loader').show();
            $.ajax({
                type: "POST",
                url: BASE_URL + "search/filtertobrandsearch",
                dataType: 'json',
                data: {'Sprice': Sprice, 'Eprice': Eprice, 'id': id, 'spm': spm},
                success: function (res) {
                    jQuery('html, body').animate({
                                scrollTop: 0
                            }, '2000', 'swing');
                    // empty(.a21);
                    if (res != null) {
                        $('.loader').hide();
                        $('.products-list-content').html(res.html);
                    } else
                        $('.products-list-content').html('asdasd');
                }
            });
        }
        $('.find1').click(function () {
            console.log('asdasd');
            var slug = '<?php echo $this->uri->segment(1) ?>';
            if ($(this).prop("checked") == true) {
                id_brand = $(this).val() + GetURLParameter('id_brand');
                var page_url = BASE_URL + slug + "/filter?id_brand=" + id_brand;
                window.history.pushState('string', '', page_url);

                FilterByBrand(id_brand,slug);

            }else if ($(this).prop("checked") == false) {
                var idbran = null;
                var id_2 = $(this).val() + "-";
                var id_3 = "-" + $(this).val();
                var id_4 = $(this).val();
                var aaa = GetURLParameter2('id_brand');

                var id_brand2 = aaa.replace(id_2, "");
                var id_brand3 = id_brand2.replace(id_3, "");
                var id_brand = id_brand3.replace(id_4, "");
                var page_url = BASE_URL + slug + "/filter?id_brand=" + id_brand;
                console.log(page_url);
                window.history.pushState('string', '', page_url);
                 FilterByBrand(id_brand,slug);
            }

        });
        $('.btn_filterpr').click(function () {
            var slug = '<?php echo $this->uri->segment(1) ?>';
            var id_brand = GetURLParameter('id_brand');
            var Startprice = $('.Startprice123').val();
            var Endprice = $('.Endprice123').val();
            var page_url = BASE_URL + slug + "/filter?id_brand=" + id_brand + "&Startprice=" + Startprice + "&Endprice=" + Endprice;
            window.history.pushState('string', '', page_url);
            FilterByPrice(Startprice,Endprice,id_brand,slug);
        });
        $('.find-search').click(function () {

            spm = '<?php echo $this->input->get('spm'); ?>';
            if ($(this).prop("checked") == true) {
                id_brand = $(this).val() + GetURLParameter('id_brand');
                var page_url = BASE_URL + "search?spm=" + spm + "&id_brand=" + id_brand;
                window.history.pushState('string', '', page_url);
                SearchByBrand(id_brand,spm);
            } else if ($(this).prop("checked") == false) {
                var idbran = null;
                var aaa = GetURLParameter2('id_brand');
                var id_2 = $(this).val() + "-";
                var id_3 = "-" + $(this).val();
                var id_4 = $(this).val();
                var id_brand2 = aaa.replace(id_2, "");
                var id_brand3 = id_brand2.replace(id_3, "");
                var id_brand = id_brand3.replace(id_4, "");
                var page_url = BASE_URL + "search?spm=" + spm + "&id_brand=" + id_brand;
                console.log(page_url);
                window.history.pushState('string', '', page_url);
                SearchByBrand(id_brand,spm);
            }
        });
        $('.btn_searchpr').click(function () {
             spm = '<?php echo $this->input->get('spm'); ?>';
            var id_brand = GetURLParameter2('id_brand');
            var Startprice = $('.StartPrice').val();
            var Endprice = $('.EndPrice').val();
            var page_url = BASE_URL + "search?spm=" + spm + "&id_brand=" + id_brand + "&Startprice=" + Startprice + "&Endprice=" + Endprice;
            window.history.pushState('string', '', page_url);
            SearchByPrice(Startprice,Endprice,id_brand,spm);
        });
         $('.btn_searchpr_sp').click(function () {
             spm = '<?php echo $this->input->get('spm'); ?>';
            var id_brand = GetURLParameter2('id_brand');
            var Startprice = $('.StartPriceSp').val();
            var Endprice = $('.EndPriceSp').val();
            var page_url = BASE_URL + "search?spm=" + spm + "&id_brand=" + id_brand + "&Startprice=" + Startprice + "&Endprice=" + Endprice;
            window.history.pushState('string', '', page_url);
            SearchByPrice(Startprice,Endprice,id_brand,spm);
        });

    });
</script>
<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution="setup_tool"
     page_id="188890125062353"
     logged_in_greeting="Xin chào! Bạn cần tư vấn?"
     logged_out_greeting="Xin chào! Bạn cần tư vấn?">
</div>
</body>
</html>