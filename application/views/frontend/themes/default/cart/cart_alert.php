<div class="">
	<div class="col-md-6">
		<div class="">
			<div class="cart-alert-message"><i class="fa fa-check-circle"></i> 1 sản phẩm đã được thêm vào giỏ hàng của bạn</div>
		</div>
		<?php
$featured_image = NULL;

foreach ($product->images as $key => $image) {
	if ($image->featured == 'Yes') {
		$featured_image = $image;
		break;
	}
}
?>
		<div class="row">
			<div class="col-md-6"><img src="<?php echo $featured_image->path; ?>"></div>
			<div class="col-md-6">
				<div class="cart-alert-name"><?php echo $product->name ?></div>
				<div class="cart-alert-price"><?php echo number_format($this->product_model->SalesProductByID($product->id), 0, ',', '.'); ?> ₫</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 cart-alert-cart-info">
		<div class="cart-alert-my-cart">Giỏ hàng của bạn (<?php echo $this->cart->total_items(); ?> sản phẩm) </div>
		<div class="row">
			<div class="col-md-6">Tạm tính</div>
			<div class="col-md-6 text-right"><?php echo number_format($this->cart->total(), 0, ',', '.'); ?> ₫</div>
			<div class="col-md-12"><hr style="margin-bottom: 10px;margin-top: 10px;"></div>
			<div class="col-md-6">Tổng cộng</div>
			<div class="col-md-6 text-right cart-alert-price">
				<?php echo number_format($this->cart->total(), 0, ',', '.'); ?> ₫
				<div>Đã bao gồm VAT (nếu có)</div>

			</div>
		</div>
		<div class="row" style="margin-top: 10px;">
			<div class="col-md-6">
				<a class="btn btn-default" href="<?php echo site_url('cart') ?>">Đến giỏ hàng</a>
			</div>
			<div class="col-md-6 text-right ">
				<a class="btn btn-warning" href="<?php echo site_url('checkout') ?>">Thanh toán</a>
			</div>
		</div>
	</div>
</div>