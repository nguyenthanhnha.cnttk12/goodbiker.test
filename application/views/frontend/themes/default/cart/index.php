<section class="main-content">
    <div class="content-wrapper">
        <div class="container">
            <?php if ($this->cart->total_items() <= 0) { ?>
                <div class="text-center">Không có sản phẩm nào trong giỏ hàng.</div>
                <div class="text-center" style="margin: 30px auto;"><a class="btn btn-warning" href="<?php echo site_url(); ?>">Tiếp tục mua hàng</a></div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-md-8 bg-light">
                        <h3 class="cart-index-heading">Giỏ hàng</h3>
                        <form action="<?php echo site_url('cart/update') ?>" >
                            <table class="table ">
                                <thead class="hidden-sm hidden-xs">
                                    <tr>
                                        <th>Sản phẩm</th>
                                        <th></th>
                                        <th width="15%">Giá</th>
                                        <th width="18%">Số lượng</th>
                                        <th width="15%">Tạm tính</th>
                                    </tr>							
                                </thead>
                                <tbody>
                                    <?php foreach ($this->cart->contents() as $items) { ?> 							
                                        <tr id="<?php echo $items['rowid']; ?>"  class="hidden-sm hidden-xs">
                                            <td><img class="cart-index-img img-thumbnail" src="<?php echo $items['image']->path ?>"></td>
                                            <td><?php echo $items['name'] ?>
                                                <br />
                                                <a class="remove-from-cart" data-rowid="<?php echo $items['rowid'] ?>" title="Xóa khỏi giỏ hàng"><i class="fa fa-trash"></i></a>
                                            </td>
                                            <td class="cart-index-price text-right"><?php echo number_format($items['price'], 0, ',', '.'); ?> ₫</td>
                                            <td>
                                                <div class="input-group"> 
                                                    <a class="input-group-addon decrease-qty cart-index-update-qty" data-target="qty<?php echo $items['id'] ?>" data-rowid="<?php echo $items['rowid'] ?>"><i class="fa fa-minus"></i></a> 
                                                    <input id="qty<?php echo $items['id'] ?>" name="qty[<?php echo $items['id'] ?>]" aria-label="Số lượng" class="form-control input-sm text-center qty<?php echo $items['id'] ?>" value="<?php echo $items['qty'] ?>" maxlength="2"  /> 
                                                    <a class="input-group-addon increase-qty cart-index-update-qty" data-target="qty<?php echo $items['id'] ?>" data-rowid="<?php echo $items['rowid'] ?>" ><i class="fa fa-plus"></i></a> 
                                                </div>
                                            </td>
                                            <td class="text-right"><span id="subtotal_<?php echo $items['rowid'] ?>" class="cart-index-price"><?php echo number_format($items['price'] * $items['qty'], 0, ',', '.'); ?> ₫</span></td>
                                        </tr>
                                        <!-- SP -->
                                        <tr class="visible-sm visible-xs">
                                            <td><img class="cart-index-img img-thumbnail" src="<?php echo $items['image']->path ?>"></td>
                                            <td>
                                                <?php echo $items['name'] ?> <a class="remove-from-cart" data-rowid="<?php echo $items['rowid'] ?>" title="Xóa khỏi giỏ hàng"><i class="fa fa-trash"></i></a>
                                                <br />
                                                <?php echo number_format($items['price'], 0, ',', '.'); ?> ₫
                                                <br />
                                                <div class="input-group"  style="width: 50%;"> 
                                                    <a class="input-group-addon decrease-qty cart-index-update-qty" data-target="qty<?php echo $items['id'] ?>" data-rowid="<?php echo $items['rowid'] ?>"><i class="fa fa-minus"></i></a> 
                                                    <input id="qty<?php echo $items['id'] ?>" name="qty[<?php echo $items['id'] ?>]" aria-label="Số lượng" class="form-control input-sm text-center qty<?php echo $items['id'] ?>" value="<?php echo $items['qty'] ?>" maxlength="2" /> 
                                                    <a class="input-group-addon increase-qty cart-index-update-qty" data-target="qty<?php echo $items['id'] ?>" data-rowid="<?php echo $items['rowid'] ?>" ><i class="fa fa-plus"></i></a> 
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <div class="col-md-4 ">
                        <div class="bg-light col-md-12">
                            <h3 class="cart-index-heading">Thông tin đơn hàng</h3>
                            <div class="row cart-index-order-row">
                                <div class="col-md-6">Tạm tính (<?php echo $this->cart->total_items() ?> sản phẩm)</div>
                                <div class="col-md-6 text-right"><span id="cart-index-subtotal"><?php echo number_format($this->cart->total(), 0, ',', '.'); ?> ₫</span></div>
                            </div>
                            <!--<div class="row cart-index-order-row">
                                <div class="col-md-6">Phí giao hàng </div>
                                <div class="col-md-6 text-right">Miễn phí</div>
                            </div>-->
                            <div class="row cart-index-order-row">
                                    <!-- <div class="col-md-8"><input type="text" name="discount_code" value="" class="form-control"> </div>
                                    <div class="col-md-4 text-right"><a class="btn btn-info">Áp dụng</a></div> -->
                                <div class="col-md-12">
                                    <div class="input-group"> <input aria-describedby="basic-addon2" placeholder="" name="discount_code" class="form-control"> <a id="basic-addon2" class="input-group-addon btn btn-info">Áp dụng</a> </div>
                                </div>
                            </div>
                            <div class="row cart-index-order-row">
                                <div class="col-md-6">Tổng cộng </div>
                                <div class="col-md-6 text-right">
                                    <span id="cart-index-total" class=" cart-index-price"><?php echo number_format($this->cart->total(), 0, ',', '.'); ?> ₫</span>
                                    <br>
                                    <small>Đã bao gồm VAT (nếu có)</small>
                                </div>
                            </div>
                            <div class=" cart-index-order-row">
                                <a class="btn btn-warning " href="<?php echo site_url('checkout'); ?>" style="display: block;">Tiến hành thanh toán</a>
                            </div>
                        </div>
                    </div>

                </div>
            <?php } ?>
        </div>
    </div>
</section>