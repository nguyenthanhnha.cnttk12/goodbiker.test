<section class="main-content">
    <div class="content-wrapper product-detal-page">
        <div class="container">
            <div class="breadcrumb hidden-sm hidden-xs"><?php echo $breadcrumbs; ?></div>
            <div class="row">
                <div class="product-content col-md-10  col-sm-8  col-xs-12"  >
                    <div class="row">
                        <div class="col-md-4  col-sm-6  col-xs-12">
                            <!-- <div class="product-detail-image">
                                    <img src="<?php //echo $featured_image->path  ?>" alt="<?php // echo $product->name;  ?>">
                                </div>	 -->
                                <div class="sp-loading"><img src="<?php echo site_url('assets/smoothproducts/css/sp-loading.gif') ?>" alt=""><br>LOADING IMAGES</div>
                                <div class="sp-wrap">
                                    <a href="<?php echo $featured_image->path ?>"><img src="<?php echo $featured_image->path ?>"></a>
                                    <?php
foreach ($other_images as $key => $image) {
	echo '<a href="' . $image->path . '"><img src="' . $image->path . '"></a>';
}
?>
                               </div>
                           </div>
                           <div class="col-md-8 col-sm-6  col-xs-12">
                            <div class="product-summary">
                                <div class="product-detail-brand"><h3><span class="label label-danger"><?php echo $product->brand; ?></span></h3></div>
                                <h1 class="product-category-heading"><?php echo $product->name; ?></h1>
                                <div class="product-summary-details">
                                    <div class="product-detail-price">
                                        <del style="font-size: 12px;color: #333;"><?php echo number_format($product->price, 0, ',', '.'); ?> ₫</del>
                                        <div class="product-tile-price"><?php echo number_format($this->product_model->SalesProductByID($product->id), 0, ',', '.'); ?> ₫</div>
                                    </div>
                                    <div class="product-buying">
                                        <a class="btn btn-lg btn-warning add-to-cart " data-productid="<?php echo $product->id; ?>"><i class="fa fa-cart-plus"></i> Thêm vào giỏ</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="product-detail-description">
                            <h2 class="product-detail-description-heading">Mô tả sản phẩm <span class="hidden-sm hidden-xs"><?php echo $product->name ?></span></h2>
                            <div class="product-detail-description-tab"><?php echo $product->description; ?></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="product-detail-bestview">
                            <h3>Những sản phẩm bạn xem nhiều nhất</h3>
                            <div class="product-bestview-list row">
                                <?php load_element($this->theme_path . 'product/detail-review');?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-relative col-md-2  col-sm-4  col-xs-12" style="padding-left: 20px;background-color: #f6f6f6;">
                    <h4 style="border-bottom: 1px dotted #777;font-weight: bold; padding: 10px 0;">Quảng cáo sản phẩm</h4>
                    <div class="row detail-list">
                        <?php load_element($this->theme_path . 'product/detail-list');?>
                    </div>
                </div>


            </div>
           <!--  <div class="visible-xs visible-sm text-center">
                <a class="btn btn-warning add-to-cart visible-xs visible-sm" data-productid="<?php echo $product->id; ?>"><i class="fa fa-cart-plus"></i> Thêm vào giỏ</a>
            </div> -->
        </div>
    </div>
    <!-- <div class="content-wrapper">
        <div class="container">

        </div>
    </div> -->
</section>