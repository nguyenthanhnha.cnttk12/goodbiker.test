<?php foreach ($products as $product) { ?>
    <div class="col-xs-6 col-sm-6 col-md-3 product-tile-wrapper" itemscope>
        <div class="product-tile">
            <a class="" href="<?php echo getProductUrl($product, $current_category->id) ?>" itemprop="url">
                <meta itemprop="brand" content="<?php echo $product['brand'] ?>" />
                <meta itemprop="productId" content="<?php echo $product['brand'] ?>" />
                <meta itemprop="image" content="<?php echo site_url($product['brand']); ?>" />
                <div class="product-image-wrapper">
                    <img src="<?php echo site_url($product['brand']); ?>" class="img-thumbnail" alt="<?php echo $product['brand'] ?>" />
                </div>
                <div class="product-tile-name"><?php echo $product['brand'] ?></div>
                <meta itemprop="productId" content="<?php echo $product['brand'] ?>" />
                <div>
                    <meta content="<?php echo number_format($product['brand'], 0, ',', '.'); ?> ₫" itemprop="price">
                    <meta content="VND" itemprop="priceCurrency">
                    <meta content="http://schema.org/InStock" itemprop="availability">
                    <div class="product-tile-price">
                        <del><?php echo number_format($product->price, 0, ',', '.'); ?> ₫?></del>
                        <h2><?php echo number_format($this->product_model->SalesProductByID($product->id), 0, ',', '.'); ?> ₫</h2>
                    </div>
                </div>
            </a>
            <div class="product-tile-buttons text-center">
                <button type="button" data-productid="<?php echo $product['brand']; ?>" class="btn btn-xs add-to-cart"><i class="fa fa-cart-plus"></i> Thêm vào giỏ</button>
            </div>
        </div>
    </div>
<?php
}?>